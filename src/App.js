import logo from './logo.svg';
import skyline from './city-skyline.jpg';
import cntower from './cn-tower.jpg';
import rogerscenter from './rogers-center.jpg';
import cityhall from './new-city-hall.jpg';
import rom from './rom.jpg';
import oldcityhall from './old-city-hall.jpg';
import './App.css';
import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter, faFacebook, faLinkedin, faInstagram} from '@fortawesome/free-brands-svg-icons'


//.  const images = [skyline, oldcityhall, cntower, rom, rogerscenter, cityhall];
  

function App() {
  
  const images = [
    { name: <strong>Toronto</strong>, subname: "The Skyline", photo: skyline },
    { name:  <strong>Old City Hall</strong>, subname: "The Clock Tower", photo: oldcityhall },
    { name:  <strong>The Cn Tower</strong>, subname: "One of the worlds tallest towers", photo: cntower },
    { name: <strong>Royal Ontario Museum</strong>, subname: "Most Visted Museum in Canada", photo: rom },
    { name:  <strong>The Rogers center</strong>, subname: "Toronto's iconic Skydome", photo: rogerscenter },
    { name:  <strong>Toronto City Hall</strong>, subname: "A Brutalist Architecture Masterpiece", photo: cityhall },
  
  ];
  
  const [selectedImage, setSelectedImage] = useState(null);


  return (
    <div className="App">
      <header className="App-header">
        <p class="title">
          The City of <strong>Toronto </strong>
        </p>
        <p class="subtitle">
          Historical Sites & <strong>More  </strong>
        </p>
      </header>

      <section class="grid">
       {images.map((image) => (
         <div key={image.name} class="img-div">
           <div class="img-overlay">
             <p>{image.name}</p>
             <p>{image.subname}</p>


           </div>
      
          <img
           src={image.photo}
            alt="logo"
             onClick={() => setSelectedImage(image.photo)} 
             />
             </div>
             
       ))}

  

      </section>


      <footer className="foot">
        
      <a href="https://twitter.com/cityoftoronto?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor/"> <FontAwesomeIcon icon={faTwitter}></FontAwesomeIcon></a>
      
      <a href="https://www.facebook.com/cityofto/"> <FontAwesomeIcon icon={faFacebook}></FontAwesomeIcon></a>

      <a href="https://www.linkedin.com/"> <FontAwesomeIcon icon={faLinkedin}></FontAwesomeIcon></a>
     
      <a href="https://www.instagram.com/cityofto/?hl=en/"> <FontAwesomeIcon icon={faInstagram}></FontAwesomeIcon></a>
   
        
      </footer>

      <div id='overlay' style={{ visibility: selectedImage ? 'visible' : 'hidden' }}>
        <h1><a class="close" onClick={() => setSelectedImage(null)}>X</a></h1>
        <img src={selectedImage} />
      </div>
    </div>

    
  );
 
}



export default App;


